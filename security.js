const jwt = require('jsonwebtoken')
const asis = require('./asistance')

/**
 *it create a new token 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.CreateToken = (req, res, next) => {
    const user = { email: req.body.email }
    req.token = jwt.sign({ user }, process.env.SECRET_CODE)
    next()
}

/**
 * It validate a token
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.validateToken = (req, res, next) => {
    const headToken = req.headers['auth-jwt']
    if (typeof headToken !== 'undefined') {
        const token = headToken.split(" ")[0]
        jwt.verify(token, process.env.SECRET_CODE, (err, ans) => {
            if (err) {
                res.status(401).json(asis.answerMessage(-1, 'invalid token', err))
            } else {
                next()
            }
        })
    } else {
        res.status(400).json(asis.answerMessage(-1, 'request without token', {}))
    }
}
