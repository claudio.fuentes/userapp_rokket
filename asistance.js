/**
 * it make a standard answer to requests
 * @param {Number} [code] answer code 
 * @param {string} [message] answer message
 * @param {JSON} [data] data about the request, if it haven´t information, give it a empty json
 */
exports.answerMessage = (code,message,data) => {
    return{
        code,
        message,
        data
    }
}