// módulos utilizados
const user = require('../models/users')
const md5 = require('md5')
const asis = require('../asistance')


/**
 * it validate username (email) and it password.return access JSON web token
 * @param {JSON} req 
 * @param {JSON} res 
 * @param {*} next 
 */
exports.UserValidate = async(req,res,next) => {
    try {
        const _user = await user.find({email:req.body.email,password:md5(req.body.password)})
        console.log(_user)
    if (_user.length == 1) {
        res.status(200).json(asis.answerMessage(1,"done",{token:req.token,user:_user}))    
    }else{
        res.status(401).json(asis.answerMessage(-1,'user or password invalid',{}))
    }
    } catch (error) {
        res.status(500).json(asis.answerMessage(-1,'internal error',error))   
    }
    
}

/**
 * it return all users created
 * @param {*} req 
 * @param {*} res 
 */
exports.SelectAll = async (req, res) => {
    try {
        const _user = await user.find();
        res.status(200).json(asis.answerMessage(1,'done',{count:_user.length,list:_user}))
    } catch (error) {
        res.status(500).json(asis.answerMessage(-1,'internal server error',{errorMessage : error}))
    }
}

/**
 * create a new user
 * @param {*} req 
 * @param {*} res 
 */
exports.addUser = async (req, res) => {
    const { email, password, names, lastName, phoneNumbers, gender } = req.body
    const _user = new user({
        email,
         password : md5(password),
          names, 
          lastName, 
          phoneNumbers, 
          gender
    })
    await _user.save()
    res.status(200).json(asis.answerMessage(1,`user saved with ID: ${_user.id}`,{}))
}