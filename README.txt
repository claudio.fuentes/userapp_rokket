project description: application made only to use as a test for employee process in Rokket Labs by Claudio Fuentes.

the application is very easy. You have only three functions.

Firts function is create a new user, for it you most call 'localhost:3012/signup' by http protocol using a POST method. 
On this call send in JSON format the following information: email,password,names,lastName,phoneNumbers,gender (all theese information are string type). 
If you did all good, the application return you a code and a message with the ID user created.

Then, you should use login method. It is calling 'localhost:3012/login'; this is a POST type method, so yo should send the following information on the body in JSON format:email,password (both are string).
it's going to returning the information's user and a token. Remember to copy this token and paste in the next method.

Finally you will use the user list method. To access this method you have to login first; when you are logged, copy the token that return the login method.
the user list method is a get type method, so you don't send a body; the token you may paste in the header's petition using the name : auth-jwt. 
Then you call 'localhost:3012/user' and this method will return the user list information and the count of this

additional information:
● all password are hashed by MD5
● all API returns have been formated on the next order : code (1 if all ok, -1 if some are worse), message (message about the request), data (only when there is some information that are return)
● all methods and variables are in english (it's not the best english but that is all)
● port, security code and string connection are saved in an eviorement file, it's for safety.
● this application don't have unitary testing because it didn't work (sorry for this)
● all information are updated in git repository except node_modules, for this you just clone this app and then use "npm install"


following the questions for me.

Questions
● What is the difference between JWT and OAuth authentication?
the main difference  between is the JWT is a function that make a token and validate it late into the server but OAuth create a token outside the server only if the client give him permission.
● Explain how asymmetric encryption works.
Asymmetric encryption works using two keys (public and private), the public key is general knowledge and allows the encryption to be carried out openly, however the private key is the exclusive 
knowledge of who has the authorization to decrypt and is the only one that allows the decryption. 
In this way one can ensure that only the authorized ones can access the encrypted information.
● What are the main differences between a GraphQL and REST API?
the main difference is in how they work. REST API is based on the functionalities that the backend requires, being on the one hand more specific and at the same time more limited; 
GraphQL works as a query service that intermediates the database, the client can define what specific data it requires, and GraphQL is documented while it is being developed.