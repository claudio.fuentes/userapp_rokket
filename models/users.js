const {Schema} = require('mongoose')
const mongoose = require('../connection')

const userSch = new Schema({
    email : {type:String,required:true},
    password:{type:String,required:true},
    names:{type:String,required:true},
    lastName:{type:String,required:false},
    phoneNumbers:{type:String,required:false},
    gender:{type:String,required:false}
})

module.exports = mongoose.model("userModel",userSch)