//config required
require('dotenv').config()

// modules required
const express = require('express')
const app = express()
const port = process.env.PORT_CONNECTION
const bodyParser = require("body-parser")
const morgan = require('morgan')

//app config
app.use(morgan('dev'))
app.use(express.json())
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(require('./router'))

//starting application
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})
