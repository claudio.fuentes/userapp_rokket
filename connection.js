const mongoose = require('mongoose')

// const stringConnection = "mongodb://localhost/rokket"
const stringConnection = process.env.STRING_CONNECTION

mongoose.connect(stringConnection,{useNewUrlParser:true,useUnifiedTopology:true})
    .then(db => console.log(`connection is ready`))
    .catch(err => console.log(`the following error has occurred:${err}`))

module.exports = mongoose