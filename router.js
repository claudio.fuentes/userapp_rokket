const router = require('express').Router()
const userCtrl = require('./Controller/User')
const security = require('./security')

router.get('/',(req,res) => {
    res.status(200).json({
        message:"access sucess",
        hiperMessage:"'You can understand my english... well done!'"
    })
    // res.send('You can understand my english... well done!')
})

router.get('/user',security.validateToken,userCtrl.SelectAll)
router.post('/signup',userCtrl.addUser)
router.post('/login',security.CreateToken,userCtrl.UserValidate)

module.exports = router